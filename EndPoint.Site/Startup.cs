using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Interfaces.FacadPatterns;
using Mahoor_store.Application.Services.Carts;
using Mahoor_store.Application.Services.Categories.FacadPattern;
using Mahoor_store.Application.Services.Common;
using Mahoor_store.Application.Services.Common.Queries.GetHomePageImages;
using Mahoor_store.Application.Services.Common.Queries.GetSliders;
using Mahoor_store.Application.Services.Finances;
using Mahoor_store.Application.Services.Finances.Queries.GetRequestPayForAdmin;
using Mahoor_store.Application.Services.HomePage.Commands.AddNewHomePageImage;
using Mahoor_store.Application.Services.HomePage.Commands.AddNewSlider;
using Mahoor_store.Application.Services.Orders.Commands.AddNewOrder;
using Mahoor_store.Application.Services.Orders.Queries.GetOrders;
using Mahoor_store.Application.Services.Orders.Queries.GetOrdersForAdmin;
using Mahoor_store.Application.Services.Users.Commands.ChangeUserStatus;
using Mahoor_store.Application.Services.Users.Commands.LoginUser;
using Mahoor_store.Application.Services.Users.Commands.RegisterUser;
using Mahoor_store.Application.Services.Users.Commands.RemoveUser;
using Mahoor_store.Application.Services.Users.Queries.GetRoles;
using Mahoor_store.Application.Services.Users.Queries.GetUsers;
using Mahoor_store.Common.Roles;
using Mahoor_store.Persistence.Contexts;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EndPoint.Site
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAuthentication(option =>
            {
                option.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                option.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                
            }).AddCookie(option =>
            {
                option.LoginPath = new PathString("/Authenticaton/Signin");
                option.ExpireTimeSpan = TimeSpan.FromMinutes(5.0);
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy(UserRoles.Admin, policy => policy.RequireRole(UserRoles.Admin));
                options.AddPolicy(UserRoles.Customer, policy => policy.RequireRole(UserRoles.Customer));
                options.AddPolicy(UserRoles.Operator, policy => policy.RequireRole(UserRoles.Operator));
            });
            services.AddScoped<ILoginUserService, LoginUserService>();
            services.AddScoped<IGetUsers, GetUsersService>();
            services.AddScoped<IDatabaseContext, DatabaseContext>();
            services.AddScoped<IGetRoles , GetRolesService>();
            services.AddScoped<IRegisterUserService, RegisterUserService>();
            services.AddScoped<IRemoveUserService, RemoveUserService>();
            services.AddScoped<IChangeUserStatus, ChangeUserStatus>();
            services.AddScoped<IAddNewSliderService, AddNewSliderService>();
            services.AddScoped<IMenuItemService, MenuItemService>();
            services.AddScoped<IGetSlidersService, GetSlidersService>();
            services.AddScoped<IAddNewHomePageImageService, AddNewHomePageImageService>();
            services.AddScoped<IGetHomePageImagesService, GetHomePageImageService>();
            services.AddScoped<ICartService, CartService>();
            services.AddScoped<IAddRequestPayService, AddRequestPayService>();
            services.AddScoped<IGetRequestPayService, GetRequestPayService>();
            services.AddScoped<IAddNewOrderService, AddNewOrderService>();
            services.AddScoped<IGetOrderService, GetOrderService>();
            services.AddScoped<IGetOrderForAdminService, GetOrderForAdminService>();
            services.AddScoped<IGetRequestPayForAdminService, GetRequestPayForAdminService>();

            services.AddScoped<IProductFacad, ProductFacad>();
            string conn = @"Data Source=DESKTOP-CE40QUT\MAHOOR;Initial Catalog=Mahoor_store;Integrated Security=True;";

            services.AddEntityFrameworkSqlServer().AddDbContext<DatabaseContext>(option =>
                option.UseSqlServer(Configuration.GetConnectionString("MainDatabase")));
            
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name : "areas",
                    pattern : "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
