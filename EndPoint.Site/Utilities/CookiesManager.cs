﻿using System;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace EndPoint.Site.Utilities
{
    public class CookiesManager
    {

        public void Add(HttpContext context, string token, string value)
        {
            context.Response.Cookies.Append(token , value , GetCookiesOption(context));
        }

        public bool IsContain(HttpContext context, string token)
        {
            return context.Request.Cookies.ContainsKey(token);
        }

        public string GetValue(HttpContext context, string token)
        {
            string cookieValue ;
            if (!context.Request.Cookies.TryGetValue(token , out cookieValue))
            {
                return null;
            }

            return cookieValue;
        }

        public void Remove(HttpContext context , string token)
        {
            if (context.Request.Cookies.ContainsKey(token))
            {
                context.Response.Cookies.Delete(token);
            }
        }

        private CookieOptions GetCookiesOption(HttpContext context)
        {
            return new CookieOptions()
            {
                Expires = DateTime.Now.AddDays(100) ,
                Path = context.Request.PathBase.HasValue ? context.Request.PathBase.ToString() : "/" ,
                HttpOnly = true ,
                Secure = context.Request.IsHttps
                
            };
        }

        public Guid GetBrowserId(HttpContext context)
        {
            string browserId;
            Guid result;
            
            if (IsContain(context,"BrowserID" ))
            {
                browserId = GetValue(context, "BrowserID");

                result = Guid.Parse(browserId);

                return result;
            }
            
            result = Guid.NewGuid();
            browserId = result.ToString();
            Add(context , "BrowserID" , browserId);
            return result;

        }
        
    }
}