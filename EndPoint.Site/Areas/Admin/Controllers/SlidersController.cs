﻿using Mahoor_store.Application.Services.HomePage.Commands.AddNewSlider;
using Mahoor_store.Domain.Entities.Products;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SlidersController : Controller
    {
        private readonly IAddNewSliderService _addNewSlider;

        public SlidersController(IAddNewSliderService addNewSlider)
        {
            _addNewSlider = addNewSlider;
        }
        
        
        [HttpPost]
        public IActionResult AddNewSlider(IFormFile file , string Link)
        {
            return Json(_addNewSlider.Execute(file, Link));
        }
    }
}