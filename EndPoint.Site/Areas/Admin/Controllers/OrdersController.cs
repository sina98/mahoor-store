﻿using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Application.Services.Orders.Queries.GetOrdersForAdmin;
using Mahoor_store.Common;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class OrdersController : Controller
    {

        private readonly IGetOrderForAdminService _getOrder;

        public OrdersController(IGetOrderForAdminService getOrder)
        {
            _getOrder = getOrder;
        }
        
        public IActionResult Index(OrderState orderState)
        {

            var result = _getOrder.Execute(orderState);
            return Json(result);
        }
    }
}