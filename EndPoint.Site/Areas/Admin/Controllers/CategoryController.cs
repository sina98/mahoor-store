﻿using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Interfaces.FacadPatterns;
using Mahoor_store.Application.Services.Categories.Commands.AddNewCategory;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {

        private readonly IProductFacad _productFacad;

        public CategoryController(IProductFacad productFacad)
        {

            _productFacad = productFacad;
        }    
        
        [HttpPost]
        public IActionResult AddNewCategory(string name, long? parentID)
        {
            var result = _productFacad.AddNewCategoryService.Execute(parentID, name);
            return Json(result);
        }

        [HttpGet]
        public IActionResult GetCategories(long? parentId)
        {
            var result = _productFacad.GetCategoriesService.Execute(parentId);
            return Json(result);
        }
    }
}