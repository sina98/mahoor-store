﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using EndPoint.Site.Models.ViewModels;
using Mahoor_store.Application.Services.Users.Commands.ChangeUserStatus;
using Mahoor_store.Application.Services.Users.Commands.RegisterUser;
using Mahoor_store.Application.Services.Users.Commands.RemoveUser;
using Mahoor_store.Application.Services.Users.Queries.GetRoles;
using Mahoor_store.Application.Services.Users.Queries.GetUsers;
using Mahoor_store.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UserController : Controller
    {

        private readonly IGetUsers _getUsers;

        private readonly IGetRoles _getRoles;

        private readonly IRegisterUserService _register;

        private readonly IRemoveUserService _removeUser;

        private readonly IChangeUserStatus _changeUserStatus;

        public UserController(IGetUsers getUsers , IGetRoles getRoles , IRegisterUserService register , IRemoveUserService removeUser , IChangeUserStatus changeUserStatus)
        {
            _getUsers = getUsers;

            _getRoles = getRoles;

            _register = register;

            _removeUser = removeUser;

            _changeUserStatus = changeUserStatus;
        }
        
       
        [HttpGet]
        public IActionResult Index(string SearchKey , int page)
        {
            return View(_getUsers.Execute(new RequestGetUserDto()
            {
                Page = page ,
                
                SearchKey = SearchKey
            }));
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Roles = new SelectList(_getRoles.Execute().Data ,"Id" , "Name");
            return View();
        }
        
        [HttpPost]
        public IActionResult Create( UserViewModel request )
        {
            ResultDto<ResultRegisterUserDto> results = _register.Execute(new RequestRegisterUserDto()
            {
                Email = request.Email ,
                Password = request.Password ,
                RePassword = request.RePassword ,
                PhoneNum = request.PhoneNum ,
                FName = request.FName ,
                LName = request.LName ,
                Roles = new List<RolesInRegisterUserDto>()
                {
                    new RolesInRegisterUserDto()
                    {
                        Id = request.Role
                    }
                } 

            });

            return Json(results);
        }

        [HttpPost]
        public IActionResult Remove(long userId)
        {
            return Json(_removeUser.Execute(userId));
        }

        [HttpPost]
        public IActionResult ChangeStatus(long userId)
        {
            return Json(_changeUserStatus.Execute(userId));
        }
        
    }
}