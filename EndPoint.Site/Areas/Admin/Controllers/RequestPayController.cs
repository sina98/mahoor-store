﻿using Mahoor_store.Application.Services.Finances.Queries.GetRequestPayForAdmin;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RequestPayController : Controller
    {

        private readonly IGetRequestPayForAdminService _getRequests;

        public RequestPayController(IGetRequestPayForAdminService getRequests)
        {
            _getRequests = getRequests;
        }
        
        public IActionResult Index()
        {
            var result = _getRequests.Execute();
            return Json(result);
        }
    }
}