﻿using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET
        [Area("Admin")]
        public IActionResult Index()
        {
            return View();
        }
    }
}