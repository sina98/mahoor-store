﻿using System.Collections.Generic;
using Mahoor_store.Application.Interfaces.FacadPatterns;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    
    [Area("Admin")]
    public class ProductController : Controller
    {

        private readonly IProductFacad _productFacad;

        public ProductController(IProductFacad productFacad)
        {
            _productFacad = productFacad;
        }

        
        
        [HttpGet]
        public IActionResult Index(int Page = 1, int pagesize = 20)
        {
            return Json(_productFacad.GetProductForAdminService.Execute(Page, pagesize).Data.Products);
        }
        
        [HttpGet]
        public IActionResult AddNewProduct()
        {
            ViewBag.Categories = new SelectList(_productFacad.GetAllCategoriesService.Execute().Data , "Id" , "Name");
            return View();
        }
        
        
        [HttpPost]
        public IActionResult AddNewProduct(RequestAddNewProductDto request , List<AddNewFeatureDto> features)
        {
            List<IFormFile> images = new List<IFormFile>();
            features = new List<AddNewFeatureDto>();
            features.Add(new AddNewFeatureDto()
            {
                Value = "رم" ,
                DisplayName = "4 گیگابایت"
            });
            features.Add(new AddNewFeatureDto()
            {
                Value = "سی پی یو" ,
                DisplayName = "Core i 7"
            });

            for (int i = 0; i < Request.Form.Files.Count; i++)
            {
                var file = Request.Form.Files[i];
                images.Add(file);
            }

            request.Images = images;
            request.Features = features;
            
            var result = _productFacad.AddNewProductService.Execute(request);
            return Json(result);
        }

        [HttpGet]
        public IActionResult GetProductDetail(long Id)
        {
            return Json(_productFacad.GetProductDetailForAdminService.Execute(Id).Data);
        }
    }
}