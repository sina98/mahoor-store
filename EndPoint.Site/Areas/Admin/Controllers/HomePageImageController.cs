﻿using Mahoor_store.Application.Services.HomePage.Commands.AddNewHomePageImage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomePageImageController : Controller
    {
        private readonly IAddNewHomePageImageService _homePageImage;

        public HomePageImageController(IAddNewHomePageImageService homePageImage)
        {
            _homePageImage = homePageImage;
        }
        
        
        [HttpPost]
        public IActionResult Add(IFormFile file , RequestAddNewHomePageImage_Dto request)
        {
            return Json(_homePageImage.Execute(file , request));
        }
    }
}