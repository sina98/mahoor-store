﻿using System.Collections.Generic;
using Mahoor_store.Application.Services.Users.Commands.RegisterUser;

namespace EndPoint.Site.Models.ViewModels
{
    public class UserViewModel
    {
        public string LName { get; set; }
        public string FName { get; set; }
        public string Email{ get; set; }
        public string PhoneNum { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public long   Role { get; set; }
    }
}