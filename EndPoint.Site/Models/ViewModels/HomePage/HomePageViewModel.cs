﻿using System.Collections.Generic;
using Mahoor_store.Application.Services.Common.Queries.GetHomePageImages;
using Mahoor_store.Application.Services.Common.Queries.GetSliders;
using Mahoor_store.Domain.Entities.Home_Page;

namespace EndPoint.Site.Models.ViewModels.HomePage
{
    public class HomePageViewModel
    {
        public List<SliderDto> Sliders { get; set; }

        public List<HomePageImages_Dto> HomePageImages { get; set; }
    }
}