﻿using EndPoint.Site.Utilities;
using Mahoor_store.Application.Services.Orders.Queries.GetOrders;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Controllers
{
    public class OrderController : Controller
    {
        private readonly IGetOrderService _getOrder;

        public OrderController(IGetOrderService getOrder)
        {
            _getOrder = getOrder;
        }

        public IActionResult Index(int page , int pageSize)
        {

            long? userId = ClaimsUtility.GetUserId(User);
            var result = _getOrder.Execute(userId.Value, page, pageSize);
            return Json(result) ;
        }
    }
}