﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using EndPoint.Site.Models.ViewModels;
using FluentValidation.Results;
using Mahoor_store.Application.Services.Users.Commands.LoginUser;
using Mahoor_store.Application.Services.Users.Commands.RegisterUser;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
namespace EndPoint.Site.Controllers
{
    public class AuthenticationController:Controller
    {
        private readonly ILoginUserService _login;
        private readonly IRegisterUserService _register;

        public AuthenticationController(ILoginUserService login , IRegisterUserService register)
        {
            _login = login;
            _register = register;
        }
        
        
        [HttpGet]
        public IActionResult SignUp()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult SignUp(UserViewModel request)
        {
            var resultRegister = _register.Execute(new RequestRegisterUserDto()
            {
                Email = request.Email,
                Password = request.Password,
                RePassword = request.RePassword,
                FName = request.FName,
                LName = request.LName,
                PhoneNum = request.PhoneNum,
                Roles = new List<RolesInRegisterUserDto>()
                {
                    new RolesInRegisterUserDto()
                    {
                        Id = 3
                    }
                }
            });

            if (resultRegister.IsSuccess)
            {
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.NameIdentifier , resultRegister.Data.UserId.ToString()) ,
                    new Claim(ClaimTypes.Email , request.Email),
                    new Claim(ClaimTypes.Name , request.FName + request.LName) ,
                    new Claim(ClaimTypes.Role , "Customer")
                };
                 
                var identity = new ClaimsIdentity(claims , CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                var properties = new AuthenticationProperties()
                {
                    IsPersistent = true , 
                    ExpiresUtc = DateTime.Now.AddDays(5)
                };
                HttpContext.SignInAsync(principal, properties);
            }

            return Json(resultRegister);
        }
        

        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }
        
        
        [HttpPost]
        public IActionResult SignIn(RequestLoginUserDto request)
        {
            var loginResult = _login.Execute(request);

            if (loginResult.IsSuccess)
            {
                 var claims = new List<Claim>()
                 {
                     new Claim(ClaimTypes.NameIdentifier , loginResult.Data.UserId.ToString()) ,
                     new Claim(ClaimTypes.Email , request.UserName),
                     new Claim(ClaimTypes.Name , loginResult.Data.FName + loginResult.Data.LName) ,
                 };

                 foreach (var ITEM in loginResult.Data.Roles)
                 {
                     claims.Add(new Claim(ClaimTypes.Role , ITEM));
                 }
                 var identity = new ClaimsIdentity(claims , CookieAuthenticationDefaults.AuthenticationScheme);
                 var principal = new ClaimsPrincipal(identity);
                 var properties = new AuthenticationProperties()
                 {
                     IsPersistent = true , 
                     ExpiresUtc = DateTime.Now.AddDays(5)
                 };
                 HttpContext.SignInAsync(principal, properties);
            }
            
            return Json(loginResult);
            
        }
    }
}