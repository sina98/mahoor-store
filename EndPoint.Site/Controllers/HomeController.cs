﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EndPoint.Site.Models;
using EndPoint.Site.Models.ViewModels.HomePage;
using Mahoor_store.Application.Services.Common.Queries.GetHomePageImages;
using Mahoor_store.Application.Services.Common.Queries.GetSliders;
using Mahoor_store.Common;

namespace EndPoint.Site.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IGetSlidersService _getSliders;
        private readonly IGetHomePageImagesService _homePageImages;

        public HomeController(ILogger<HomeController> logger ,  IGetSlidersService getSliders ,
            IGetHomePageImagesService homePageImages)
        {
            _logger = logger;
            _getSliders = getSliders;
            _homePageImages = homePageImages;
        }

        public IActionResult Index()
        {
            HomePageViewModel homePage = new HomePageViewModel()
            {
                Sliders = _getSliders.Execute().Data,
                HomePageImages = _homePageImages.Execute().Data
            };

            return View(homePage);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
