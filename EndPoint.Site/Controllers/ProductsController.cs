﻿using Mahoor_store.Application.Interfaces.FacadPatterns;
using Mahoor_store.Application.Services.Products.Queries.GetProductForSite;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Controllers
{
    public class ProductsController : Controller
    {

        private readonly IProductFacad _productFacad;

        public ProductsController(IProductFacad productFacad)
        {
            _productFacad = productFacad;
        }
        
        [HttpGet]
        public IActionResult Index(OrderSort orderSort ,string? searchKey , long? catId , int page = 1 , int pageSize = 5)
        {
           return Json(_productFacad.GetProductForSiteService.Execute(orderSort,page , pageSize,searchKey , catId ).Data);
        }

        [HttpGet]
        public IActionResult Detail(long id)
        {
           return Json(_productFacad.GetProductDetailForSiteService.Execute(id).Data);
        }
    }
}