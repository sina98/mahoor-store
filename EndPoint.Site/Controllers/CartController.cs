﻿using EndPoint.Site.Utilities;
using Mahoor_store.Application.Services.Carts;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;
using Microsoft.AspNetCore.Mvc;

namespace EndPoint.Site.Controllers
{
    public class CartController : Controller
    {

        private readonly ICartService _cartService;
        private readonly CookiesManager cookiesManager;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
            cookiesManager = new CookiesManager();
        }
        
        public IActionResult Index()
        {
            var userId = ClaimsUtility.GetUserId(User);
            var result = _cartService.GetMyCart(cookiesManager.GetBrowserId(HttpContext) , userId);
            
            return Json(result);
        }

        [HttpPost]
        public IActionResult AddToMyCart(long productID)
        {
            var userId = ClaimsUtility.GetUserId(User);
            
            _cartService.AddToCart(cookiesManager.GetBrowserId(HttpContext), productID , userId);
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult RemoveFromCart(long productId)
        {
            _cartService.RemoveFromCart(cookiesManager.GetBrowserId(HttpContext), productId);

            return RedirectToAction("Index");
        }

        public IActionResult Add(long cartItemId)
        {
            var result = _cartService.AddCount(cookiesManager.GetBrowserId(HttpContext), cartItemId);

            return Json(result);
        }

        public IActionResult LowOff(long cartItemId)
        {
            var result = _cartService.LowOffCount(cookiesManager.GetBrowserId(HttpContext), cartItemId);
            return Json(result);
        }
    }
}