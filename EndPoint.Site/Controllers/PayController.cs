﻿using System;
using System.Threading.Tasks;
using Dto.Payment;
using EndPoint.Site.Utilities;
using Mahoor_store.Application.Services.Carts;
using Mahoor_store.Application.Services.Finances;
using Mahoor_store.Application.Services.Orders.Commands.AddNewOrder;
using Mahoor_store.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ZarinPal.Class;

namespace EndPoint.Site.Controllers
{
    //[Authorize("Customer")]
    public class PayController : Controller
    {

        private readonly IAddRequestPayService _requestPay;
        private readonly IGetRequestPayService _getRequestPay;
        private readonly ICartService _cartService;
        private readonly CookiesManager _cookiesManager;
        private readonly Payment _payment;
        private readonly Authority _authority;
        private readonly Transactions _transactions;
        private readonly IAddNewOrderService _addNewOrder;
        
        public PayController(IAddRequestPayService requestPay , ICartService cartService 
        ,  IGetRequestPayService getRequestPay , IAddNewOrderService addNewOrder )
        {
            _requestPay = requestPay;
            _getRequestPay = getRequestPay;
            _cartService = cartService;
            _cookiesManager = new CookiesManager();
            var expose = new Expose();
            _payment = expose.CreatePayment();
            _authority = expose.CreateAuthority();
            _transactions = expose.CreateTransactions();
            _addNewOrder = addNewOrder;
        }
        
        
        public async Task<IActionResult> Index()
        {
            long? userId = ClaimsUtility.GetUserId(User);

            var cart = _cartService.GetMyCart(_cookiesManager.GetBrowserId(HttpContext), userId);

            if (cart.Data.SumAmount > 0)
            {
                var requestPay =  _requestPay.Execute(cart.Data.SumAmount, userId.Value);

                var result = await _payment.Request(new DtoRequest()
                {
                    Mobile = requestPay.Data.Phone,
                    CallbackUrl = $"https://localhost:5001/pay/verify?guid={requestPay.Data.Guid}",
                    Description = "توضیحات",
                    Email = requestPay.Data.Email,
                    Amount = requestPay.Data.Amount,
                    MerchantId = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
                }, ZarinPal.Class.Payment.Mode.sandbox);
                return Redirect($"https://sandbox.zarinpal.com/pg/StartPay/{result.Authority}");
            }

            return RedirectToAction("Index", "Cart");
        }

        public async Task<IActionResult> Verify(Guid guid, string authority , string status)
        {
            var result = _getRequestPay.Execute(guid);
             var verification = await _payment.Verification(new DtoVerification
                {
                    Amount = result.Data.Amount,
                    MerchantId = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
                    Authority = authority
                }, Payment.Mode.sandbox);

             long? userId = ClaimsUtility.GetUserId(User);
             var cart = _cartService.GetMyCart(_cookiesManager.GetBrowserId(HttpContext), userId);
             
             if (verification.Status == 100)
             {
                 _addNewOrder.Execute(new RequestAddNewOrder_Dto()
                 {
                     CartId = cart.Data.CartId ,
                     UserId = userId.Value ,
                     RequestPayId = result.Data.RequestPayId
                 });
             }
             else
             {
                 
             }
                return View();
        }
    }
}