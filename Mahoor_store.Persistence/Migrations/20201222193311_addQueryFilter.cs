﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Mahoor_store.Persistence.Migrations
{
    public partial class addQueryFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "InsertTime",
                value: new DateTime(2020, 12, 22, 23, 3, 10, 581, DateTimeKind.Local).AddTicks(1079));

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2L,
                column: "InsertTime",
                value: new DateTime(2020, 12, 22, 23, 3, 10, 584, DateTimeKind.Local).AddTicks(5799));

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3L,
                column: "InsertTime",
                value: new DateTime(2020, 12, 22, 23, 3, 10, 584, DateTimeKind.Local).AddTicks(5914));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1L,
                column: "InsertTime",
                value: new DateTime(2020, 12, 22, 22, 54, 30, 947, DateTimeKind.Local).AddTicks(780));

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2L,
                column: "InsertTime",
                value: new DateTime(2020, 12, 22, 22, 54, 30, 950, DateTimeKind.Local).AddTicks(5088));

            migrationBuilder.UpdateData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3L,
                column: "InsertTime",
                value: new DateTime(2020, 12, 22, 22, 54, 30, 950, DateTimeKind.Local).AddTicks(5207));
        }
    }
}
