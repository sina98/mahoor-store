﻿using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Domain.Entities.Home_Page
{
    public class HomePageImage : BaseEntity<long>
    {
        public string Src { get; set; }

        public string Link { get; set; }

        public HomePageImageLocation Location { get; set; }

        public int ClickCount { get; set; }
    }

    public enum HomePageImageLocation
    {
        MidTop = 0 ,
        Mid1 = 1,
        Mid2 = 2,
        Mid3 = 3,
        Mid4 = 4,
        R1 = 5 ,
        R2 = 6 ,
        R3 = 7 ,
        R4 = 8 ,
        R5 = 9 ,
        R6 = 10 ,
        R7 = 11,
        MidButtom1 = 12 ,
        MidButtom2 = 13
        
        
    }
}