﻿using System.Runtime;
using Mahoor_store.Domain.Entities.Common;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Mahoor_store.Domain.Entities.Home_Page
{
    public class Slider : BaseEntity<long>
    {
        public string Src { get; set; }
        public string Link { get; set; }
        
        public int ClickCount { get; set; }
    }
}