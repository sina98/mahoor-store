﻿using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Domain.Entities.Users
{
    public class UserInRole : BaseEntityWithoutId
    {
        public long Id{ get; set; }

        public virtual User User { get; set; }

        public long UserId { get; set; }

        public virtual Role Role { get; set; }

        public long RoleId { get; set; }
    }
}