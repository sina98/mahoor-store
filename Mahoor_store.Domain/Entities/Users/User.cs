﻿using System.Collections.Generic;
using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Domain.Entities.Users
{
    public class User : BaseEntityWithoutId
    {
        public long Id { get; set; }

        public string LName { get; set; }

        public string FName { get; set; }

        public string Email{ get; set; }

        public string PhoneNum { get; set; }

        public string Password { get; set; }

        public ICollection<UserInRole> UserInRoles { get; set; }
        
        public virtual ICollection<Order> Orders { get; set; }
        
        
    }
}