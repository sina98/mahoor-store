﻿using System.Collections.Generic;
using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Domain.Entities.Users
{
    public class Role : BaseEntityWithoutId
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public ICollection<UserInRole> UserInRoles { get; set; }
        
    }
}