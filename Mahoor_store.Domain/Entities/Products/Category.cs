﻿using System.Collections.Generic;
using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Domain.Entities.Products
{
    public class Category : BaseEntityWithoutId
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public virtual Category ParentCategory { get; set; }

        public long? ParentCategoryId { get; set; }

        public virtual ICollection<Category> SubCategory { get; set; }
    }
}