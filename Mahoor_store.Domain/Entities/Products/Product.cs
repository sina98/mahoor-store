﻿using System.Collections.Generic;
using System.Runtime;
using Mahoor_store.Domain.Entities.Common;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Mahoor_store.Domain.Entities.Products
{
    public class Product : BaseEntity<long>
    {

        public string Name { get; set; }

        public string Brand { get; set; }

        public string Description { get; set; }

        public string Model { get; set; }
        
        public string Code { get; set; }

        public int Price { get; set; }

        public int Number { get; set; }

        public int ProductRate { get; set; }

        public int ProductView { get; set; }
        public bool IsDisplayed { get; set; }

        public bool IsExist { get; set; }
        
        public virtual Category Category { get; set; }

        public long CategoryId { get; set; }

        public virtual ICollection<ProductImage> Images { get; set; }

        public virtual ICollection<ProductFeature> Features { get; set; }
        
        
    }
}