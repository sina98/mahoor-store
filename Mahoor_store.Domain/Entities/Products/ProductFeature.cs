﻿using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Domain.Entities.Products
{
    public class ProductFeature : BaseEntity<long>
    {
        public virtual Product Product { get; set; }

        public long ProductId { get; set; }

        public string DisplayName { get; set; }

        public string Value { get; set; }
    }
}