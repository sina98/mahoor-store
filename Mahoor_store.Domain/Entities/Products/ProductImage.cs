﻿using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Domain.Entities.Products
{
    public class ProductImage : BaseEntity<long>
    {
        public virtual Product Product { get; set; }

        public long ProductId { get; set; }

        public string Src { get; set; }
        
    }
}