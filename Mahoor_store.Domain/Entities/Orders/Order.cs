﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mahoor_store.Domain.Entities.Common;
using Mahoor_store.Domain.Entities.Finances;
using Mahoor_store.Domain.Entities.Users;

namespace Bugeto_Store.Domain.Entities.Orders
{
    public class Order : BaseEntity<long>
    {

        public virtual User User { get; set; }
        public long UserId { get; set; }

        public virtual RequestPay RequestPay { get; set; }
        public long RequestPayId { get; set; }

        public OrderState OrderState { get; set; }

        public string Address { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
     }


    public enum OrderState
    {
        Processing = 0,
        Canceled = 1,
        Delivered = 2,
    }

}
