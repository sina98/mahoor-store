﻿using System;
using System.Collections.Generic;
using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Domain.Entities.Common;
using Mahoor_store.Domain.Entities.Users;

namespace Mahoor_store.Domain.Entities.Finances
{
    public class RequestPay : BaseEntity<long>
    {
        public Guid Guid { get; set; }
        public virtual User User { get; set; }

        public long UserId { get; set; }

        public int AmountPay { get; set; }

        public bool IsPay { get; set; }

        public DateTime? DateTimePay { get; set; }

        public string Authority { get; set; }

        public long RefId { get; set; } = 0;
        
        public virtual ICollection<Order> Orders { get; set; }
       
    }
}