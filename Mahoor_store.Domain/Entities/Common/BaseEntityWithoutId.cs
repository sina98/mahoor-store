﻿using System;

namespace Mahoor_store.Domain.Entities.Common
{
    public class BaseEntityWithoutId
    {
        public DateTime InsertTime { get; set; } = DateTime.Now;

        public DateTime? UpdateTime { get; set; }

        public bool IsRemoved { get; set; }

        public DateTime? RemoveTime { get; set; }

        public bool IsActivate { get; set; } = true;
        
        
    }
}