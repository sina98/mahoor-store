﻿using Mahoor_store.Domain.Entities.Common;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Domain.Entities.Carts
{
    public class CartItem : BaseEntity<long>
    {
        public virtual Product Product { get; set; }
        public long ProductId { get; set; }

        public virtual Cart Cart { get; set; }
        public long CartId { get; set; }

        public int Count { get; set; }
        public int Price { get; set; }

        public int SumPrice { get; set; }
        
    }
}