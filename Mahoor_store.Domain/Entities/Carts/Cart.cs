﻿using System;
using System.Collections.Generic;
using System.Runtime;
using Mahoor_store.Domain.Entities.Common;
using Mahoor_store.Domain.Entities.Users;

namespace Mahoor_store.Domain.Entities.Carts
{
    public class Cart :BaseEntity<long>
    {
        public virtual User  User { get; set; }
        public long? UserId { get; set; }

        public bool IsFinished { get; set; }

        public Guid BrowserId { get; set; }

        public ICollection<CartItem> CartItems { get; set; }

        public int SumAmount { get; set; }
    }
}