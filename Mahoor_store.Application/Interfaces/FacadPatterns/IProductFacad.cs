﻿using Mahoor_store.Application.Services.Categories.Commands.AddNewCategory;
using Mahoor_store.Application.Services.Categories.Queries.GetCategories;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct;
using Mahoor_store.Application.Services.Products.Queries.GetAllCategories;
using Mahoor_store.Application.Services.Products.Queries.GetProductDetailForAdmin;
using Mahoor_store.Application.Services.Products.Queries.GetProductDetailForSite;
using Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin;
using Mahoor_store.Application.Services.Products.Queries.GetProductForSite;

namespace Mahoor_store.Application.Interfaces.FacadPatterns
{
    public interface IProductFacad
    {
        AddNewCategoryService AddNewCategoryService { get; }
        
        GetCategoriesService GetCategoriesService { get; }

        AddNewProductService AddNewProductService { get; }
        
        GetAllCategoriesService GetAllCategoriesService { get; }
        
        GetProductForAdminService GetProductForAdminService { get; }
        
        GetProductDetailForAdminService GetProductDetailForAdminService { get; }
        
        GetProductForSiteService GetProductForSiteService { get; }
        
        GetProductDetailForSiteService GetProductDetailForSiteService { get; }
    }
}