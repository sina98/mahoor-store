﻿using System.Threading;
using System.Threading.Tasks;
using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Domain.Entities.Carts;
using Mahoor_store.Domain.Entities.Finances;
using Mahoor_store.Domain.Entities.Home_Page;
using Mahoor_store.Domain.Entities.Products;
using Mahoor_store.Domain.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Interfaces
{
    public interface IDatabaseContext
    {
         DbSet<User> Users { get; set; }

         DbSet<Role> Roles { get; set; }

         DbSet<UserInRole> UserInRoles { get; set; }
         
          DbSet<Category> Categories { get; set; }
         
         DbSet<Product> Products { get; set; }

         DbSet<ProductImage> ProductImages { get; set; }

         DbSet<ProductFeature> ProductFeatures { get; set; }
         
         DbSet<Slider> Sliders { get; set; }
         
         DbSet<HomePageImage> HomePageImages { get; set; }
         
         DbSet<Cart> Carts { get; set; }
         
         DbSet<CartItem> CartItems { get; set; }
         
         DbSet<RequestPay> RequestPays { get; set; }
         
         DbSet<Order> Orders { get; set; }
         
         DbSet<OrderDetail> OrderDetails { get; set; }
         

         int SaveChanges(bool acceptAllChangesOnSuccess);

         int SaveChanges();
         
         Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess , CancellationToken cancellationToken = new CancellationToken());


         Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());
    }
}