﻿using System;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Carts;
using Mahoor_store.Domain.Entities.Products;
using Mahoor_store.Domain.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Carts
{
    public class CartService : ICartService
    {
        private readonly IDatabaseContext _context;

        public CartService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto AddToCart(Guid browserId, long productId , long? userId)
        {
            
            var cart = _context.Carts.Where(p => p.BrowserId == browserId && p.IsFinished == false)
                .FirstOrDefault();

            if (cart != null && userId != null)
            {
                cart.UserId = userId;
                cart.User = _context.Users.Find(userId);
                _context.SaveChanges();
            }

            if (cart == null && userId != null)
            {
                 cart = _context.Carts
                     .Where(p => p.UserId == userId && p.IsFinished == false)
                     .FirstOrDefault();
            }
            


            if (cart == null)
            {
                Cart newCart = new Cart()
                {
                    BrowserId = browserId ,
                    IsFinished = false ,
                    InsertTime = DateTime.Now ,
                };
                if (userId != null)
                {
                    newCart.UserId = userId;
                    newCart.User = _context.Users.Find(userId);
                }

                _context.Carts.Add(newCart);
                _context.SaveChanges();

                cart = newCart;
            }

            var product = _context.Products.Find(productId);

            var cartItem = _context.CartItems
                .Where(p => p.ProductId == productId && p.CartId == cart.Id)
                .FirstOrDefault();

            if (cartItem == null)
            {
                CartItem newCartItem = new CartItem()
                {
                    Cart = cart ,
                    CartId = cart.Id ,
                    Product = product,
                    ProductId = product.Id ,
                    Price = product.Price ,
                    InsertTime = DateTime.Now ,
                    Count = 1,
                    SumPrice = product.Price
                };

                _context.CartItems.Add(newCartItem);
                
            }
            
            else
            {
                cartItem.Count++;
                cartItem.SumPrice = cartItem.Count * cartItem.Price;
                
            }
            UpdateSumAmount(cart);
            _context.SaveChanges();
            return new ResultDto()
            {
                Message = "محصول با موفقیت اضافه شد" ,
                IsSuccess = true
            };

        }

        public ResultDto RemoveFromCart(Guid browserId, long productId)
        {
            var cartItem = _context.CartItems
                .Include(p => p.Cart)
                .Where(p => p.Cart.BrowserId == browserId && p.ProductId == productId)
                .FirstOrDefault();

            if (cartItem == null)
            {
                return new ResultDto()
                {
                    Message = "Cart Item not found " ,
                    IsSuccess = false
                };
            }

            cartItem.SumPrice = 0;
            cartItem.IsRemoved = true;
            cartItem.RemoveTime = DateTime.Now;
            UpdateSumAmount(cartItem.Cart);
            _context.SaveChanges();
            return new ResultDto()
            {
                Message = "محصول با موفقیت حذف شد" ,
                IsSuccess = true
            };
        }

        public ResultDto<Cart_Dto> GetMyCart(Guid browserId , long? userId)
        {
            var cart = _context.Carts
                .Include(p => p.CartItems)
                .ThenInclude(p => p.Product)
                .ThenInclude(p => p.Images)
                .Where(p => (p.BrowserId == browserId) && p.IsFinished == false)
                .OrderByDescending(p => p.Id)
                .FirstOrDefault();

            if (cart != null && userId != null)
            {
                cart.UserId = userId;
                cart.User = _context.Users.Find(userId);
                _context.SaveChanges();
            }

            if (cart == null && userId != null)
            {
                cart = _context.Carts
                    .Include(p => p.CartItems)
                    .ThenInclude(p => p.Product)
                    .ThenInclude(p => p.Images)
                    .Where(p => p.UserId == userId && p.IsFinished == false)
                    .OrderByDescending(p => p.Id)
                    .FirstOrDefault();
            }
           

            return new ResultDto<Cart_Dto>()
            {
                Data = new Cart_Dto()
                {
                    SumAmount = cart.SumAmount,
                    CartId = cart.Id ,
                    CartItems = cart.CartItems.ToList().Select( p => new CartItem_Dto()
                    {
                        Count = p.Count,
                        Price = p.Price,
                        Product = p.Product.Name ,
                        Id = p.ProductId ,
                        Image = p.Product.Images.ToList().FirstOrDefault().Src
                    }).ToList()
                },
                Message = "Success" ,
                IsSuccess = true
            };
            
            
        }

        public ResultDto AddCount(Guid browserId, long cartItemId)
        {
            var cartItem = _context.CartItems
                .Include(p => p.Cart)
                .Where(p => p.Id == cartItemId && p.Cart.BrowserId == browserId)
                .FirstOrDefault();

            cartItem.Count++;
            
            cartItem.SumPrice = cartItem.Count * cartItem.Price;
            UpdateSumAmount(cartItem.Cart);
            _context.SaveChanges();
            
            return new ResultDto()
            {
                Message = "Success" ,
                IsSuccess = true
            };
        }

        public ResultDto LowOffCount(Guid browserId, long cartItemId)
        {
            var cartItem = _context.CartItems
                .Include(p => p.Cart)    
                .Where(p => p.Id == cartItemId && p.Cart.BrowserId == browserId)
                .FirstOrDefault();

            if (cartItem.Count <= 1)
            {
                return new ResultDto()
                {
                    Message = "Failed" ,
                    IsSuccess = false
                };
            }

            cartItem.Count--;
            cartItem.SumPrice = cartItem.Count * cartItem.Price;
            UpdateSumAmount(cartItem.Cart);
             _context.SaveChanges();
            
            return new ResultDto()
            {
                Message = "Success" ,
                IsSuccess = true
            };
        }

        private void UpdateSumAmount(Cart cart)
        {
            cart.SumAmount = 0;
            foreach (var VARIABLE in cart.CartItems)
            {
                cart.SumAmount += VARIABLE.SumPrice;
            }
        }
    }
}