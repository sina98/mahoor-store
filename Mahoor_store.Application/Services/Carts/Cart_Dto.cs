﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Carts
{
    public class Cart_Dto
    {
        public int SumAmount { get; set; }

        public long CartId { get; set; }
        public List<CartItem_Dto> CartItems { get; set; }
    }
}