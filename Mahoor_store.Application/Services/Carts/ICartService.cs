﻿using System;
using System.Collections.Generic;
using System.Runtime;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Carts
{
    public interface ICartService
    {
        ResultDto AddToCart(Guid browserId, long productId , long? userId);
        
        ResultDto RemoveFromCart(Guid browserId, long productId);

        ResultDto<Cart_Dto> GetMyCart(Guid browserId , long? userId);

        ResultDto AddCount(Guid browserId, long cartItemId);
        
        ResultDto LowOffCount(Guid browserId, long cartItemId);
    }
}
