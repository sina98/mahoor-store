﻿namespace Mahoor_store.Application.Services.Carts
{
    public class CartItem_Dto
    {
        public string Product { get; set; }

        public int Count { get; set; }

        public int Price { get; set; }

        public long Id { get; set; }

        public string Image { get; set; }

    }
}