﻿using System.Collections.Generic;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Users;

namespace Mahoor_store.Application.Services.Orders.Queries.GetOrders
{
    public interface IGetOrderService
    {
        ResultDto<List<GetOrder_Dto>> Execute(long userId, int page, int pageSize);
    }
}