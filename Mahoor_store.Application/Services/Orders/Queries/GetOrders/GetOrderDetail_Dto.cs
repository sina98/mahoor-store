﻿namespace Mahoor_store.Application.Services.Orders.Queries.GetOrders
{
    public class GetOrderDetail_Dto
    {
        public long OrderDetailId { get; set; }
        public long ProductId { get; set; }

        public int Count { get; set; }

        public int Price { get; set; }
    }
}