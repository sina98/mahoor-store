﻿using System.Collections.Generic;
using Bugeto_Store.Domain.Entities.Orders;

namespace Mahoor_store.Application.Services.Orders.Queries.GetOrders
{
    public class GetOrder_Dto
    {
        public long OrderId { get; set; }
        public long UserId { get; set; }

        public long RequestPayId { get; set; }

        public OrderState OrderState { get; set; }

        public string Address { get; set; }

        public List<GetOrderDetail_Dto> OrderDetails { get; set; }
    }
}