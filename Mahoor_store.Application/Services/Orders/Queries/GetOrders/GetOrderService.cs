﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Orders.Queries.GetOrders
{
    public class GetOrderService : IGetOrderService
    {
        
        private readonly IDatabaseContext _context;

        public GetOrderService(IDatabaseContext context)
        {
            _context = context;
        }
        
        
        public ResultDto<List<GetOrder_Dto>> Execute(long userId, int page, int pageSize)
        {
            int rowsCount;
            
            var orders = _context.Orders
                .Include(p => p.OrderDetails)
                .ThenInclude( p => p.Product)
                .Where(p => p.UserId == userId)
                .OrderByDescending(p => p.Id)
                .ToPaged(page, pageSize, out rowsCount)
                .Select(p => new GetOrder_Dto()
                {
                    OrderId = p.Id ,
                    Address = p.Address,
                    OrderState = p.OrderState,
                    UserId = p.UserId,
                    RequestPayId = p.RequestPayId,
                    OrderDetails = p.OrderDetails.Select(q => new GetOrderDetail_Dto()
                    {
                        Count = q.Count,
                        Price = q.Price,
                        OrderDetailId = q.Id,
                        ProductId = q.ProductId
                    }).ToList()
                }).ToList();
            
            return new ResultDto<List<GetOrder_Dto>>()
            {
                Data = orders ,
                Message = "Success" ,
                IsSuccess = true
            };
        }
    }
}