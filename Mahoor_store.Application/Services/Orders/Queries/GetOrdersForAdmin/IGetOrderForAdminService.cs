﻿using System.Collections.Generic;
using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Orders.Queries.GetOrdersForAdmin
{
    public interface IGetOrderForAdminService
    {
        ResultDto<List<Order_Dto>> Execute(OrderState orderState);
    }
}