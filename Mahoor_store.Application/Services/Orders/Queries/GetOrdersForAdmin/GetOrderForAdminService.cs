﻿using System.Collections.Generic;
using System.Linq;
using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Orders.Queries.GetOrdersForAdmin
{
    public class GetOrderForAdminService : IGetOrderForAdminService
    {

        private readonly IDatabaseContext _context;

        public GetOrderForAdminService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<List<Order_Dto>> Execute(OrderState orderState)
        {

            var orders = _context.Orders
                .Include(p => p.OrderDetails)
                .OrderByDescending(p => p.Id)
                .ToList()
                .Select(p => new Order_Dto()
                {
                    InsertTime = p.InsertTime,
                    OrderId = p.Id,
                    OrderState = p.OrderState,
                    ProductCount = p.OrderDetails.Count(),
                    UserId = p.UserId,
                    RequestPayId = p.RequestPayId
                }).ToList();
            
            return new ResultDto<List<Order_Dto>>()
            {
                Data = orders ,
                Message = "Success",
                IsSuccess = true
            };


        }
    }
}