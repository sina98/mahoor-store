﻿using System;
using Bugeto_Store.Domain.Entities.Orders;

namespace Mahoor_store.Application.Services.Orders.Queries.GetOrdersForAdmin
{
    public class Order_Dto
    {
        public long OrderId { get; set; }

        public long UserId { get; set; }

        public long RequestPayId { get; set; }

        public DateTime InsertTime { get; set; }

        public OrderState OrderState { get; set; }

        public int ProductCount { get; set; }
    }
}