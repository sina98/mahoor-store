﻿namespace Mahoor_store.Application.Services.Orders.Commands.AddNewOrder
{
    public class RequestAddNewOrder_Dto
    {
        public long CartId { get; set; }

        public long RequestPayId { get; set; }

        public long UserId { get; set; }
        
        public string Authority { get; set; }

        public int RefId { get; set; }
    }
}