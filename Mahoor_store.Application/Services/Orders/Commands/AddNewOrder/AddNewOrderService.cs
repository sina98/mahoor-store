﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Orders.Commands.AddNewOrder
{
    public class AddNewOrderService : IAddNewOrderService
    {

        private readonly IDatabaseContext _context;

        public AddNewOrderService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto Execute(RequestAddNewOrder_Dto request)
        {
            var user = _context.Users.Find(request.UserId);

            var requestPay = _context.RequestPays.Find(request.RequestPayId);

            var cart = _context.Carts
                .Include(p => p.CartItems)
                .ThenInclude(p => p.Product)
                .Where(p => p.Id == request.CartId)
                .FirstOrDefault();

            requestPay.IsPay = true;
            
            requestPay.DateTimePay = DateTime.Now;

            requestPay.Authority = request.Authority;

            requestPay.RefId = request.RefId;

            cart.IsFinished = true;
            
            Order order = new Order()
            {
                Address = "",
                User = user ,
                UserId = user.Id ,
                RequestPay = requestPay ,
                RequestPayId = requestPay.Id ,
                OrderState = OrderState.Processing
            };

            _context.Orders.Add(order);

            List<OrderDetail> orderDetails = new List<OrderDetail>();
            foreach (var item in cart.CartItems)
            {
                orderDetails.Add(new OrderDetail()
                {
                    Count = item.Count,
                    Price = item.Price,
                    Order = order,
                    Product = item.Product,
                    ProductId = item.Product.Id,
                    OrderId = order.Id,

                });
                
            }
            _context.OrderDetails.AddRange(orderDetails);

            _context.SaveChanges();
            
            return new ResultDto()
            {
                Message = "Success" ,
                IsSuccess = true
            };




        }
    }
}