﻿using AutoMapper.Configuration;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Orders.Commands.AddNewOrder
{
    public interface IAddNewOrderService
    {
        ResultDto Execute(RequestAddNewOrder_Dto request);
    }
}