﻿using System.Collections.Generic;
using Bugeto_Store.Domain.Entities.Orders;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Users;

namespace Mahoor_store.Application.Services.Finances.Queries.GetRequestPayForAdmin
{
    public interface IGetRequestPayForAdminService
    {
        ResultDto<List<RequestPay_Dto>> Execute();
    }
}