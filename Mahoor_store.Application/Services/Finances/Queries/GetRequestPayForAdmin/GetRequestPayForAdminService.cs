﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Finances.Queries.GetRequestPayForAdmin
{
    public class GetRequestPayForAdminService : IGetRequestPayForAdminService
    {

        private readonly IDatabaseContext _context;

        public GetRequestPayForAdminService(IDatabaseContext context)
        {
            _context = context;
        }

        public ResultDto<List<RequestPay_Dto>> Execute()
        {
            var requests = _context.RequestPays
                .Include(p => p.User)
                .ToList()
                .Select(p => new RequestPay_Dto()
                {
                    Authority = p.Authority,
                    Guid = p.Guid,
                    AmountPay = p.AmountPay,
                    IsPay = p.IsPay,
                    RefId = p.RefId,
                    UserId = p.UserId,
                    UserName = p.User.LName,
                    DateTimePay = p.DateTimePay
                }).ToList();
            
            return new ResultDto<List<RequestPay_Dto>>()
            {
                Data = requests,
                Message = "Success" ,
                IsSuccess = true
            };
        }
    }
}