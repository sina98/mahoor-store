﻿using System;

namespace Mahoor_store.Application.Services.Finances.Queries.GetRequestPayForAdmin
{
    public class RequestPay_Dto
    {
        public Guid Guid { get; set; }

        public long UserId { get; set; }

        public string UserName { get; set; }

        public int AmountPay { get; set; }

        public bool IsPay { get; set; }

        public DateTime? DateTimePay { get; set; }

        public string Authority { get; set; }

        public long RefId { get; set; } = 0;
        
    }
}