﻿using System;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Finances
{
    public class GetRequestPayService : IGetRequestPayService
    {

        private readonly IDatabaseContext _context;

        public GetRequestPayService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<RequestPay_Dto> Execute(Guid guid)
        {
            var requestPay = _context.RequestPays
                .Include(p => p.User)
                .Where(p => p.Guid == guid)
                .FirstOrDefault();
            
            return new ResultDto<RequestPay_Dto>()
            {
                Data = new RequestPay_Dto()
                {
                    Amount = requestPay.AmountPay,
                    Email = requestPay.User.Email,
                    Phone = requestPay.User.PhoneNum,
                    Guid = requestPay.Guid,
                    RequestPayId = requestPay.Id
                }
            };
            
        }
    }
}