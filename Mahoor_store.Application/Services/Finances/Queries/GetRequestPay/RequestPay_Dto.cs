﻿using System;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Finances
{
    public class RequestPay_Dto
    {
        public Guid Guid { get; set; }

        public string Email { get; set; }

        public int Amount { get; set; }

        public string Phone { get; set; }

        public long RequestPayId { get; set; }
    }
}