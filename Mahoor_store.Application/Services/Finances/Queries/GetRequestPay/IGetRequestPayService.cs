﻿using System;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Finances
{
    public interface IGetRequestPayService
    {
        ResultDto<RequestPay_Dto> Execute(Guid guid);
    }
}