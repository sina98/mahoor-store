﻿using System;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Finances;
using Mahoor_store.Domain.Entities.Users;

namespace Mahoor_store.Application.Services.Finances
{
    public class AddRequestPayService : IAddRequestPayService
    {

        private readonly IDatabaseContext _context;

        public AddRequestPayService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<RequestPay_Dto> Execute(int amount, long userId)
        {
            User user = _context.Users.Find(userId);
            
            RequestPay request = new RequestPay()
            {
                Guid = Guid.NewGuid(),
                User = user ,
                UserId = user.Id,
                InsertTime = DateTime.Now ,
                AmountPay = amount ,
                IsPay = false ,
                
            };
            _context.RequestPays.Add(request);
            _context.SaveChanges();
            
            return new ResultDto<RequestPay_Dto>()
            {
                Data = new RequestPay_Dto()
                {
                    Amount = request.AmountPay,
                    Email = user.Email,
                    Guid = request.Guid,
                    Phone = user.PhoneNum,
                    RequestPayId = request.Id
                },
                IsSuccess = true ,
                Message = "Success"
            };
        }
    }
}