﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Finances
{
    public interface IAddRequestPayService
    {
        ResultDto<RequestPay_Dto> Execute(int amount, long userId);
    }
}