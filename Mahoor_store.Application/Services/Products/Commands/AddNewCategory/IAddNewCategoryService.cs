﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Categories.Commands.AddNewCategory
{
    public interface IAddNewCategoryService
    {
        ResultDto Execute(long? parentId, string name);
    }
}