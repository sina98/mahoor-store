﻿using FluentValidation;

namespace Mahoor_store.Application.Services.Categories.Commands.AddNewCategory.Validation
{
    public class NewCategoryValidation : AbstractValidator<string>
    {
        public NewCategoryValidation()
        {
            RuleFor(x => x.Length).NotEmpty().WithMessage("لطفا نام دسته را وارد کنید");
        }
    }
}