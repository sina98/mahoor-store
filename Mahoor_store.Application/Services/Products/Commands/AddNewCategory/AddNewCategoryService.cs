﻿using FluentValidation.Results;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Services.Categories.Commands.AddNewCategory.Validation;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Categories.Commands.AddNewCategory
{
    public class AddNewCategoryService : IAddNewCategoryService
    {
        private readonly IDatabaseContext _context;

        public AddNewCategoryService(IDatabaseContext context)
        {
            
            _context = context;
        }
        
        public ResultDto Execute(long? parentId, string name)
        {
            
            NewCategoryValidation validation = new NewCategoryValidation();

            ValidationResult result = validation.Validate(name);

            if (!result.IsValid)
            {
                
                var str = "";
                foreach (var VARIABLE in result.Errors)
                {
                    str += "،" + VARIABLE.ErrorMessage;
                }
                
                return new ResultDto()
                {
                    
                    Message = str ,
                    IsSuccess = false
                };
            }
            
            Category newCategory = new Category()
            {
                
                ParentCategory = GetParent(parentId) ,
                ParentCategoryId = parentId,
                Name = name ,
            };

            _context.Categories.Add(newCategory);
            _context.SaveChanges();
            
            return new ResultDto()
            {
                
                Message = "ثبت دسته با موفقیت انجام شد" ,
                IsSuccess = true
            };

        }

        private Category GetParent(long? parentId)
        {
            
            var parent = _context.Categories.Find(parentId);
            return parent;
        }
    }
}