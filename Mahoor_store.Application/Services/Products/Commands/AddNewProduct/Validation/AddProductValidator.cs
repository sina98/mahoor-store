﻿using FluentValidation;

namespace Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation
{
    public class AddProductValidator : AbstractValidator<RequestAddNewProductDto>
    {

        public AddProductValidator()
        {
            RuleFor(x => x.Brand).NotEmpty().WithMessage("لطفا برند محصول را وارد کنید ،");
            RuleFor(x => x.Code).NotEmpty().WithMessage("لطفا کد محصول را وارد کنید ،");
            RuleFor(x => x.Description).NotEmpty().WithMessage("لطفا توضیحات محصول را وارد کنید ،");
            RuleFor(x => x.Model).NotEmpty().WithMessage("لطفا مدل محصول را وارد کنید ،");
            RuleFor(x => x.Name).NotEmpty().WithMessage("لطفا نام محصول را وارد کنید ،");
            RuleFor(x => x.Number).NotEmpty().WithMessage("لطفا برند محصول را وارد کنید ،");
            RuleFor(x => x.Price).NotEmpty().WithMessage("لطفا قیمت محصول را وارد کنید ،");
            RuleFor(x => x.CategoryId).NotEmpty().WithMessage("لطفا دسته بندی محصول را وارد کنید ،");
            RuleFor(x => x.IsDisplayed).NotEmpty().WithMessage("لطفا وضعیت نمایش محصول را وارد کنید ،");
            
            
            
        }
        
    }
}