﻿using System;
using System.Collections.Generic;
using System.IO;
using FluentValidation.Results;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Mahoor_store.Application.Services.Products.Commands.AddNewProduct
{
    public class  AddNewProductService : IAddNewProductService
    {

        private readonly IDatabaseContext _context;
        private readonly  IHostingEnvironment _environment;

        public AddNewProductService(IDatabaseContext context , IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }


        public ResultDto Execute(RequestAddNewProductDto request)
        {
            try
            {
                AddProductValidator validator = new AddProductValidator();
                
                ValidationResult result = validator.Validate(request);
                
                if (!result.IsValid)
                {
                    string errors = "";
                
                    foreach (var VARIABLE in result.Errors)
                    {
                        errors += VARIABLE.ErrorMessage;
                    }

                    return new ResultDto()
                    {
                        Message = errors,
                        IsSuccess = false
                    };
                }

                var category = _context.Categories.Find(request.CategoryId);

                Product product = new Product()
                {
                    Brand = request.Brand,

                    Category = category,

                    CategoryId = request.CategoryId,

                    Code = request.Code,

                    Description = request.Description,

                    Model = request.Model,

                    Name = request.Name,

                    Number = request.Number,

                    Price = request.Price,

                    IsDisplayed = request.IsDisplayed
                };

                if (request.Number == 0)
                {
                    product.IsExist = false;
                }



                List<ProductImage> productImages = new List<ProductImage>();

                foreach (var VAR in request.Images)
                {
                    var uploadedResult = UploadFile(VAR);

                    productImages.Add(new ProductImage()
                    {
                        Product = product,
                        ProductId = product.Id ,
                        Src = uploadedResult.FileNameAddress
                    });

                }
                _context.ProductImages.AddRange(productImages);
                product.Images = productImages;

                List<ProductFeature> productFeatures = new List<ProductFeature>();

                foreach (var VAR in request.Features)
                {
                    productFeatures.Add(new ProductFeature()
                    {
                        Product = product,

                        Value = VAR.Value,

                        DisplayName = VAR.DisplayName
                    });
                }

                product.Features = productFeatures;
                _context.Products.Add(product);
               _context.ProductFeatures.AddRange(productFeatures);
               
                
                _context.SaveChanges();
                return new ResultDto()
                {
                    Message = "محصول با موفقیت اضافه شد" ,
                    IsSuccess = true
                };

            }
            catch (Exception ex)
            {
                return new ResultDto()
                {
                    Message = "خطایی رخ داد" ,
                    IsSuccess = false
                };
            }
        }

        private UploadDto UploadFile(IFormFile file)
        {

            if (file == null || file.Length == 0)
            {
                return new UploadDto()
                {
                    Status = false ,
                    FileNameAddress = ""
                };
            }

            string folder = $@"images\ProductImage\";
            
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, folder);
            
            if (!Directory.Exists(uploadsRootFolder))
            {
                
                Directory.CreateDirectory(uploadsRootFolder);
            }

            string fileName = DateTime.Now.Ticks + file.FileName;

            var filePath = Path.Combine(uploadsRootFolder, fileName);

            using (var fileStream = new FileStream(filePath , FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            
            return new UploadDto()
            {
                Status = true ,
                FileNameAddress = folder + fileName
            };
        }
    }
}