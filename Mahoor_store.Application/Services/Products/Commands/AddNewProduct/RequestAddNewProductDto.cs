﻿using System.Collections.Generic;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation;
using Mahoor_store.Domain.Entities.Products;
using Microsoft.AspNetCore.Http;

namespace Mahoor_store.Application.Services.Products.Commands.AddNewProduct
{
    public class RequestAddNewProductDto
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public string Description { get; set; }

        public string Model { get; set; }
        
        public string Code { get; set; }

        public long CategoryId { get; set; }

        public int Price { get; set; }

        public int Number { get; set; }

        public List<IFormFile> Images { get; set; }

        public List<AddNewFeatureDto> Features { get; set; }

        public bool IsDisplayed { get; set; }

    }
}