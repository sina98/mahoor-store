﻿namespace Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation
{
    public class AddNewFeatureDto
    {
        public string DisplayName { get; set; }

        public string Value { get; set; }
    }
}