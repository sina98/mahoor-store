﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Products.Commands.AddNewProduct
{
    public interface IAddNewProductService
    {
        ResultDto Execute(RequestAddNewProductDto request);
    }
    
    
}