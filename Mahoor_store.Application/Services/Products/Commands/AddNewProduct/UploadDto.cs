﻿namespace Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation
{
    public class UploadDto
    {
        public bool Status { get; set; }

        public string FileNameAddress { get; set; }
    }
}