﻿using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Interfaces.FacadPatterns;
using Mahoor_store.Application.Services.Categories.Commands.AddNewCategory;
using Mahoor_store.Application.Services.Categories.Queries.GetCategories;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation;
using Mahoor_store.Application.Services.Products.Queries.GetAllCategories;
using Mahoor_store.Application.Services.Products.Queries.GetProductDetailForAdmin;
using Mahoor_store.Application.Services.Products.Queries.GetProductDetailForSite;
using Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin;
using Mahoor_store.Application.Services.Products.Queries.GetProductForSite;
using Mahoor_store.Domain.Entities.Products;
using Microsoft.AspNetCore.Hosting;

namespace Mahoor_store.Application.Services.Categories.FacadPattern
{
    public class ProductFacad : IProductFacad
    {
        private readonly IDatabaseContext _context;
        private readonly IHostingEnvironment _environment;

        public ProductFacad(IDatabaseContext context , IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        private AddNewCategoryService _addNewCategory; 
            
        public AddNewCategoryService AddNewCategoryService
        {

            get
            {
                return _addNewCategory = _addNewCategory ?? new AddNewCategoryService(_context);
            }
        }

        private GetCategoriesService _getCategories;

        public GetCategoriesService GetCategoriesService
        {
            get
            {
                return _getCategories = _getCategories ?? new GetCategoriesService(_context);
            }
        }

        private AddNewProductService _addNewProduct;

        public AddNewProductService AddNewProductService
        {
            get
            {

                return _addNewProduct = _addNewProduct ?? new AddNewProductService(_context, _environment);
            }
        }

        private GetAllCategoriesService _getAllCategories;

        public GetAllCategoriesService GetAllCategoriesService
        {
            get
            {
                return _getAllCategories = _getAllCategories ?? new GetAllCategoriesService(_context);
            }
        }
        

        private GetProductForAdminService _getProductForAdmin;

        public GetProductForAdminService GetProductForAdminService
        {
            get
            {
                return _getProductForAdmin = _getProductForAdmin ?? new GetProductForAdminService(_context);
            }
        }

        private GetProductDetailForAdminService _getProductDetailForAdmin;

        public GetProductDetailForAdminService GetProductDetailForAdminService
        {
            get
            {
                return _getProductDetailForAdmin =
                    _getProductDetailForAdmin ?? new GetProductDetailForAdminService(_context);
            }
        }

        private GetProductForSiteService _getProductForSiteService;

        public GetProductForSiteService GetProductForSiteService
        {
            get
            {
                return _getProductForSiteService = _getProductForSiteService ?? new GetProductForSiteService(_context);
            }
        }

        private GetProductDetailForSiteService _getProductDetailForSiteService;

        public GetProductDetailForSiteService GetProductDetailForSiteService
        {
            get
            {
                return _getProductDetailForSiteService =
                    _getProductDetailForSiteService ?? new GetProductDetailForSiteService(_context);
            }
        }
    }
}