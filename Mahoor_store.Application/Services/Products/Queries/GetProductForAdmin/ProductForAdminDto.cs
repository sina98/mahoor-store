﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin
{
    public class GetProductForAdminDto
    {
        public int RowsCount { get; set; }

        public int PageSize { get; set; }

        public int CurrentPage { get; set; }

        public List<ProductsFormListForAdminDto> Products { get; set; }
    }
}