﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin
{
    public interface IGetProductForAdminService
    {
        ResultDto<GetProductForAdminDto> Execute(int page = 1, int pageSize = 20);
    }
}