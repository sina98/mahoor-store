﻿using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin
{
    public class ProductsFormListForAdminDto
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public string Description { get; set; }

        public string Model { get; set; }
        
        public string Code { get; set; }

        public int Price { get; set; }

        public int Number { get; set; }

        public bool IsDisplayed { get; set; }

        public bool IsExist { get; set; }
        
        public  string Category { get; set; }


        public ProductsFormListForAdminDto MapProp(Product product)
        {
            Name = product.Name;
            Brand = product.Brand;
            Category = product.Category.Name;
            Price = product.Price;
            Model = product.Model;
            Description = product.Description;
            IsDisplayed = product.IsDisplayed;
            Code = product.Code;
            Number = product.Number;
            return this;

        }
    }
}