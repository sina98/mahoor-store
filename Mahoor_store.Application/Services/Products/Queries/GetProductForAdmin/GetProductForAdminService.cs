﻿using System.Linq;
using AutoMapper;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin.AutoMapper;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin
{
    public class GetProductForAdminService : IGetProductForAdminService
    {
        private readonly IDatabaseContext _context;

        public GetProductForAdminService(IDatabaseContext context)
        {
            _context = context;
        }

        public ResultDto<GetProductForAdminDto> Execute(int page = 1, int pageSize = 20)
        {
            int rowCount = 0;
            
            //GetProductForAdminMapper mapper = new GetProductForAdminMapper();

            var products = _context.Products
                .Include(p => p.Category)
                .ToPaged(page, pageSize, out rowCount)
                .Select(p => new ProductsFormListForAdminDto().MapProp(p))
                .ToList();
            
            return new ResultDto<GetProductForAdminDto>()
            {
                Data = new GetProductForAdminDto()
                {
                    Products = products ,
                    CurrentPage = page ,
                    PageSize = pageSize,
                    RowsCount = rowCount
                }
                ,
                Message = "عملیات با موفقیت انجام شد" ,
                
                IsSuccess = true
            };
        }
    }
}