﻿using AutoMapper;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForAdmin.AutoMapper
{
    public class GetProductForAdminMapper : Profile
    {
        public GetProductForAdminMapper()
        {
            CreateMap<ProductsFormListForAdminDto , Product>();
        }
    }
}