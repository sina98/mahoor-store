﻿using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductDetailForAdmin
{
    public class GetProductDetailForAdminService : IGetProductDetailForAdminService
    {

        private readonly IDatabaseContext _context;

        public GetProductDetailForAdminService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<ProductDetailForAdminDto> Execute(long Id)
        {
            
            var product = _context.Products
                .Include(p => p.Category)
                .ThenInclude(p => p.ParentCategory)
                .Include(p => p.Features)
                .Include(p => p.Images)
                .Where(p => p.Id == Id)
                .FirstOrDefault();
            
            return new ResultDto<ProductDetailForAdminDto>()
            {
                Data = new ProductDetailForAdminDto()
                {
                    Name = product.Name ,
                    Brand = product.Brand ,
                    Category = GetCategory(product.Category) ,
                    Price = product.Price ,
                    Model = product.Model ,
                    Description = product.Description ,
                    IsDisplayed = product.IsDisplayed ,
                    Code = product.Code ,
                    Number = product.Number ,
                    Feature = product.Features.ToList().Select(p => new ProductDetailFeatureDto()
                    {
                        Id = p.Id ,
                        Value = p.Value ,
                        DisplayName = p.DisplayName
                    }).ToList() ,
                    Images = product.Images.ToList().Select(p => new ProductDetailImageDto()
                    {
                        Id = p.Id ,
                        Src = p.Src
                    }).ToList()
                },
                Message = "Success" ,
                IsSuccess = true
            };
            
        }


        private string GetCategory(Category category)
        {
            string result = category.ParentCategory != null ? $"{category.ParentCategory.Name} - " : "";

            return result += category.Name;
        }
    }
}