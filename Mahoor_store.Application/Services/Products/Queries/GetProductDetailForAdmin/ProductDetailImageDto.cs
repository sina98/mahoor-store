﻿namespace Mahoor_store.Application.Services.Products.Queries.GetProductDetailForAdmin
{
    public class ProductDetailImageDto
    {
        public long Id { get; set; }

        public string Src { get; set; }
    }
}