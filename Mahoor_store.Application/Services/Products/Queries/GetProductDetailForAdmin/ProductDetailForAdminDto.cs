﻿using System.Collections.Generic;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductDetailForAdmin
{
    public class ProductDetailForAdminDto
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public string Description { get; set; }

        public string Model { get; set; }
        
        public string Code { get; set; }

        public int Price { get; set; }

        public int Number { get; set; }

        public bool IsDisplayed { get; set; }

        public bool IsExist { get; set; }
        
        public  string Category { get; set; }

        public List<ProductDetailFeatureDto> Feature { get; set; }

        public List<ProductDetailImageDto> Images { get; set; }
        
    }
}