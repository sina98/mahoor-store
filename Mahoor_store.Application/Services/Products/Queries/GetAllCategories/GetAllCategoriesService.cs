﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Products.Queries.GetAllCategories
{
    public class GetAllCategoriesService : IGetAllCategoriesService
    {

        private readonly IDatabaseContext _context;

        public GetAllCategoriesService(IDatabaseContext context)
        {
            _context = context;
        }


        public ResultDto<List<AllCategoriesDto>> Execute()
        {

            var categories = _context
                .Categories
                .Include(x => x.ParentCategory)
                .Where(x => x.ParentCategory != null)
                .ToList()
                .Select(p => new AllCategoriesDto()
                {
                    Id = p.Id,
                    Name = $"{p.ParentCategory.Name} - {p.Name}"
                })
                .ToList();
            
            return new ResultDto<List<AllCategoriesDto>>()
            {
                Data = categories , 
                IsSuccess = true ,
                Message = "دریافت دسته ها با موفقیت انجام شد"
            };

            
        }
    }
}