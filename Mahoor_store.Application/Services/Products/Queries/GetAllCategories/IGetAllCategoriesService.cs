﻿using System.Collections.Generic;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Products.Queries.GetAllCategories
{
    public interface IGetAllCategoriesService
    {
        ResultDto<List<AllCategoriesDto>> Execute();
    }
}