﻿namespace Mahoor_store.Application.Services.Categories.Queries.GetCategories
{
    public class CategoriesDto
    {
        
        public long Id { get; set; }

        public string Name { get; set; }

        public bool HasChild { get; set; }

        public ParentCategoriesDto ParentCategory { get; set; }
    }
}