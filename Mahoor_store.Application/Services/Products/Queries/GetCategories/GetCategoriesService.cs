﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Categories.Queries.GetCategories
{
    public class GetCategoriesService : IGetCategoriesServices
    {

        private readonly IDatabaseContext _context;

        public GetCategoriesService(IDatabaseContext context)
        {

            _context = context;
        }
        
        public ResultDto<List<CategoriesDto>> Execute(long? parentId)
        {

            var categories = _context.Categories
                .Include(p => p.ParentCategory)
                .Include(p => p.SubCategory)
                .Where(p => p.ParentCategoryId == parentId)
                .ToList()
                .Select(p => new CategoriesDto()
                {
                    Id = p.Id,

                    Name = p.Name,

                    ParentCategory = p.ParentCategory != null
                        ? new ParentCategoriesDto()
                        {
                            Id = p.ParentCategory.Id,

                            Name = p.ParentCategory.Name
                        }
                        : null,

                    HasChild = p.SubCategory.Count > 0 ? true : false

                }).ToList();
            
            return new ResultDto<List<CategoriesDto>>()
            {
                Data = categories ,
                
                Message = "دریافت دسته ها با موفقیت انجام شد" ,
                
                IsSuccess = true
            };

        }
    }
}