﻿using System.Collections.Generic;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Categories.Queries.GetCategories
{
    public interface IGetCategoriesServices
    {
        ResultDto<List<CategoriesDto>> Execute(long? parentId);
    }
}