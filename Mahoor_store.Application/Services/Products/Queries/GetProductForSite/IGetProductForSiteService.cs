﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForSite
{
    public interface IGetProductForSiteService
    {
        ResultDto<ResultProdcutForSiteDto> Execute(OrderSort orderSort,int page, int pageSize , string? searchKey , long? catId);
    }

    public enum OrderSort
    {
        //Sorting
        NoOrder = 0 ,
        MostView = 1,
        MostSelling = 2 ,
        MostRate = 3
    }
}