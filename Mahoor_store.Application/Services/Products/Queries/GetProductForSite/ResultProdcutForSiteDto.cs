﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForSite
{
    public class ResultProdcutForSiteDto
    {

        public List<ProductForSiteDto> Products { get; set; }

        public int TotalRows { get; set; }
        
        
    }
}