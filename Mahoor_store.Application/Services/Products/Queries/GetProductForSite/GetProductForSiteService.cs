﻿using System;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductForSite
{
    public class GetProductForSiteService : IGetProductForSiteService
    {

        private readonly IDatabaseContext _context;

        public GetProductForSiteService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<ResultProdcutForSiteDto> Execute(OrderSort orderSort,int page, int pageSize , string? searchKey , long? catId)
        {

            int totalRows;

            var productQuery = _context.Products
                .Include(p => p.Images).AsQueryable();

            if (catId != null)
            {
                productQuery = productQuery.Where(p => p.CategoryId == catId).AsQueryable();
            }

            if (!string.IsNullOrWhiteSpace(searchKey))
            {
                productQuery = productQuery.Where(p =>
                    p.Name.Equals(searchKey) || p.Brand.Equals(searchKey) || p.Model.Equals(searchKey)).AsQueryable();
            }
            
           
            
            //set sort on produtcs
            switch (orderSort)
            {
                case OrderSort.NoOrder :
                    break;
                case OrderSort.MostRate :
                    productQuery = productQuery.OrderByDescending(p => p.ProductRate).AsQueryable();
                    break;
                case OrderSort.MostSelling :
                    //implement later
                    break;
                case OrderSort.MostView :
                    productQuery = productQuery.OrderByDescending(p => p.ProductView).AsQueryable();
                    break;
                
            }
           
            var products = productQuery
                .ToPaged(page, pageSize, out totalRows);
            
            Random rd = new Random();
            return new ResultDto<ResultProdcutForSiteDto>
            {
                Data = new ResultProdcutForSiteDto()
                {
                    TotalRows = totalRows,
                    Products = products.Select(p => new ProductForSiteDto
                    {
                        Id = p.Id,
                        Star = rd.Next(1, 5),
                        Name = p.Name,
                        ImageSrc = p.Images.FirstOrDefault().Src,
                        Price=p.Price
                    }).ToList(),
                },
                IsSuccess = true,
            };

        }
    }
}