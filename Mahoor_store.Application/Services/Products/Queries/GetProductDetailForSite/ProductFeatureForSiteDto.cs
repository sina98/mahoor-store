﻿namespace Mahoor_store.Application.Services.Products.Queries.GetProductDetailForSite
{
    public class ProductFeatureForSiteDto
    {
        public string DisplayName { get; set; }

        public string Value { get; set; }
    }
}