﻿using System;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductDetailForSite
{
    public interface IGetProductDetailForSiteService
    {
        ResultDto<ProductDetailForSiteDto> Execute(long id);
        
    }
}