﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductDetailForSite
{
    public class ProductDetailForSiteDto
    {
        public long Id { get; set; }
        
        public string Name { get; set; }

        public string Brand { get; set; }

        public string Description { get; set; }

        public string Model { get; set; }
        
        public string Code { get; set; }

        public int Price { get; set; }

        public int Number { get; set; }

        public virtual string Category { get; set; }

        public virtual List<string> Images { get; set; }

        public List<ProductFeatureForSiteDto> Features { get; set; }

    }
}