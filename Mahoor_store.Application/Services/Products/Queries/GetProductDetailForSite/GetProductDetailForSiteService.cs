﻿using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Products.Queries.GetProductDetailForSite
{
    public class GetProductDetailForSiteService : IGetProductDetailForSiteService
    {
        private readonly IDatabaseContext _context;

        public GetProductDetailForSiteService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<ProductDetailForSiteDto> Execute(long id)
        {

            var product = _context.Products
                .Include(p => p.Images)
                .Include(p => p.Category)
                .ThenInclude(p => p.ParentCategory)
                .Include(p => p.Features)
                .Where(p => p.Id == id).FirstOrDefault();

            if (product == null)
            {
                return new ResultDto<ProductDetailForSiteDto>()
                {
                    Data = null ,
                    Message = "Product not found !" ,
                    IsSuccess = false
                };
            }

            product.ProductView++;
            _context.SaveChanges();
            
            return new ResultDto<ProductDetailForSiteDto>()
            {
                Data = new ProductDetailForSiteDto()
                {
                    Brand = product.Brand ,
                    Code = product.Code,
                    Category = $"{product.Category.ParentCategory.Name} - {product.Category.Name}",
                    Description = product.Description,
                    Id = product.Id,
                    Images = product.Images.Select(p=> p.Src).ToList(),
                    Model = product.Model,
                    Name = product.Name,
                    Price = product.Price,
                    Number = product.Number,
                    Features = product.Features.Select(p => new ProductFeatureForSiteDto()
                    {
                        Value = p.Value,
                        DisplayName = p.DisplayName
                    }).ToList(),

                },
                Message = "Success",
                IsSuccess = true
            };
            
            
        } 
    }
}