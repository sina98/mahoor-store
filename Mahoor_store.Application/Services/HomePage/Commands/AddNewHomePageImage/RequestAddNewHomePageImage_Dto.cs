﻿using Mahoor_store.Domain.Entities.Home_Page;

namespace Mahoor_store.Application.Services.HomePage.Commands.AddNewHomePageImage
{
    public class RequestAddNewHomePageImage_Dto
    {
        public string Link { get; set; }

        public HomePageImageLocation Location { get; set; }
    }
}