﻿using System;
using System.IO;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Services.Products.Commands.AddNewProduct.Validation;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Home_Page;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace Mahoor_store.Application.Services.HomePage.Commands.AddNewHomePageImage
{
    public class AddNewHomePageImageService : IAddNewHomePageImageService
    {

        private readonly IDatabaseContext _context;

        private readonly IHostingEnvironment _environment;

        public AddNewHomePageImageService(IDatabaseContext context , IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        
        public ResultDto Execute(IFormFile file,RequestAddNewHomePageImage_Dto request)
        {

            var uploadResult = UploadFile(file);

            if (!uploadResult.Status)
            {
                return new ResultDto()
                {
                    Message = "Failed",
                    IsSuccess = false
                };
            }
            
            HomePageImage image = new HomePageImage()
            {
                Link = request.Link , 
                Location = request.Location ,
                Src = uploadResult.FileNameAddress
            };

            _context.HomePageImages.Add(image);
            _context.SaveChanges();
            
            return new ResultDto()
            {
                Message = "Success" ,
                IsSuccess = true
            };




        }
        
        private UploadDto UploadFile(IFormFile file)
        {

            if (file == null || file.Length == 0)
            {
                return new UploadDto()
                {
                    Status = false ,
                    FileNameAddress = ""
                };
            }

            string folder = $@"images\HomePage\Images\";
            
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, folder);
            
            if (!Directory.Exists(uploadsRootFolder))
            {
                
                Directory.CreateDirectory(uploadsRootFolder);
            }

            string fileName = DateTime.Now.Ticks.ToString() + file.FileName;

            var filePath = Path.Combine(uploadsRootFolder, fileName);

            using (var fileStream = new FileStream(filePath , FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            
            return new UploadDto()
            {
                Status = true ,
                FileNameAddress = folder + fileName
            };
        }
        
        
    }
}