﻿using Mahoor_store.Common;
using Microsoft.AspNetCore.Http;

namespace Mahoor_store.Application.Services.HomePage.Commands.AddNewHomePageImage
{
    public interface IAddNewHomePageImageService
    {
        ResultDto Execute(IFormFile file , RequestAddNewHomePageImage_Dto request);
    }
}