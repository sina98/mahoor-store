﻿using Mahoor_store.Common;
using Microsoft.AspNetCore.Http;

namespace Mahoor_store.Application.Services.HomePage.Commands.AddNewSlider
{
    public interface IAddNewSliderService
    {
        ResultDto Execute(IFormFile file, string Link);
    }
}