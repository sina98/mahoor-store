﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Users.Commands.LoginUser
{
    public class ResultLoginUserDto
    {
        public long UserId { get; set; }

        public List<string> Roles { get; set; }

        public string FName { get; set; }
        
        public string LName { get; set; }
        
    }
}