﻿using FluentValidation;

namespace Mahoor_store.Application.Services.Users.Commands.LoginUser
{
    public class RequestLoginUserDto
    {
        public string UserName{ get; set; }

        public string Password { get; set; }
    }
}