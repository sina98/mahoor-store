﻿using FluentValidation;
using Mahoor_store.Application.Services.Users.Commands.RegisterUser;

namespace Mahoor_store.Application.Services.Users.Commands.LoginUser.Validation
{
    public class LoginUserValidator : AbstractValidator<RequestLoginUserDto>
    {
        public LoginUserValidator()
        {
            RuleFor(x => x.UserName).NotEmpty().EmailAddress().WithMessage("نام کاربری را به درستی وارد کنید !");
            RuleFor(x => x.Password).NotEmpty().WithMessage("لطفا پسورد را وارد کنید !");
        }
    }
}