﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Users.Commands.LoginUser
{
    public interface ILoginUserService
    {
        ResultDto<ResultLoginUserDto> Execute(RequestLoginUserDto request);
    }
}