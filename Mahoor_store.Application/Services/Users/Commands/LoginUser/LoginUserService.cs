﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentValidation;
using FluentValidation.Results;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Services.Users.Commands.LoginUser.Validation;
using Mahoor_store.Common;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Users.Commands.LoginUser
{
    public class LoginUserService : ILoginUserService
    {

        private readonly IDatabaseContext _context;

        private LoginUserValidator _validator;

        public LoginUserService(IDatabaseContext context)
        {
            _context = context;
        }
        
        
        public ResultDto<ResultLoginUserDto> Execute(RequestLoginUserDto request)
        {
            

            _validator = new LoginUserValidator();

            ValidationResult resultValidation = _validator.Validate(request);

            if (!resultValidation.IsValid)
            {
                string errors = "";
                
                foreach (var FAILURE in resultValidation.Errors)
                {
                    errors += "،" + FAILURE.ErrorMessage;
                }
                
                return new ResultDto<ResultLoginUserDto>()
                {
                    Data = new ResultLoginUserDto() ,
                    
                    Message = errors ,
                    
                    IsSuccess = false
                };
            }

            var user = _context.Users.Include(p => p.UserInRoles)
                .ThenInclude(p => p.Role)
                .Where(p => p.Email.Equals(request.UserName) && p.IsActivate == true)
                .FirstOrDefault();
            

            if (user == null)
            {
                return new ResultDto<ResultLoginUserDto>()
                {
                    Data = new ResultLoginUserDto() ,
                    
                    Message = "کاربری با این نام کاربری وجود ندارد !" ,
                    
                    IsSuccess = false
                };
            }

            bool verify = BCrypt.Net.BCrypt.Verify(request.Password, user.Password);

            if (!verify)
            {
                return new ResultDto<ResultLoginUserDto>()
                {
                    Data = new ResultLoginUserDto() ,
                    
                    Message = "پسورد وارد شده اشتباه است" ,
                    
                    IsSuccess = false
                };
            }

            List<string> roles = new List<string>();

            foreach (var ITEM in user.UserInRoles)
            {
                roles.Add(ITEM.Role.Name);
            }
            
            return new ResultDto<ResultLoginUserDto>()
            {
                Data = new ResultLoginUserDto()
                {
                    Roles = roles ,
                    
                    FName = user.FName ,
                    
                    LName = user.LName ,
                    
                    UserId = user.Id
                },
                    
                Message = "ورود با موفقیت انجام شد" ,
                    
                IsSuccess = true
            };

        }
    }
}
