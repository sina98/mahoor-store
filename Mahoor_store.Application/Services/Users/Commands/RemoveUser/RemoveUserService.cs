﻿using System;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Users;

namespace Mahoor_store.Application.Services.Users.Commands.RemoveUser
{
    public class RemoveUserService : IRemoveUserService
    {
        private readonly IDatabaseContext _context;

        public RemoveUserService(IDatabaseContext context)
        {
            _context = context;
        }
        
        
        public ResultDto Execute(long id)
        {
            
            var user = _context.Users.Find(id);

            if (user == null)
            {
                return new ResultDto()
                {
                    Message = "کاربر یاقت نشد" ,
                    
                    IsSuccess = false
                };
            }
            
            
                user.IsRemoved = true;
                
                user.RemoveTime =DateTime.Now;

                _context.SaveChanges();
                
                return new ResultDto()
                {
                    Message = "کاربر با موفقیت حذف شد" ,
                    
                    IsSuccess = true
                };
            
            
        }
    }
}