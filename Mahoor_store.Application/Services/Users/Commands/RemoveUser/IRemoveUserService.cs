﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Users.Commands.RemoveUser
{
    public interface IRemoveUserService
    {
        ResultDto Execute(long id);
    }
}