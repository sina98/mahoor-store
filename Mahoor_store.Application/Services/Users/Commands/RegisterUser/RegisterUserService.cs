﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using FluentValidation;
using FluentValidation.Results;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Services.Users.Commands.RegisterUser.Validation;
using Mahoor_store.Application.Services.Users.Queries.GetUsers;
using Mahoor_store.Common;

using Mahoor_store.Domain.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Users.Commands.RegisterUser
{
    public class RegisterUserService : IRegisterUserService
    {
        private readonly IDatabaseContext _context;

        private RegisterUserValidator _validator;

        public RegisterUserService(IDatabaseContext context)
        {
            _context = context;
        }

        public ResultDto<ResultRegisterUserDto> Execute(RequestRegisterUserDto requestRegister)
        {
            try
            {
                _validator = new RegisterUserValidator();

                _validator.ValidateAndThrow(requestRegister);

                string hashedPass = BCrypt.Net.BCrypt.HashPassword(requestRegister.RePassword);
                
                User user = new User()
                {
                    Email = requestRegister.Email.Trim(),

                    FName = requestRegister.FName.Trim(),

                    LName = requestRegister.LName.Trim(),

                    PhoneNum = requestRegister.PhoneNum.Trim(),

                    Password = hashedPass

                };

                List<UserInRole> userInRoles = new List<UserInRole>();

                foreach (var ITEM in requestRegister.Roles)
                {
                    var role = _context.Roles.Find(ITEM.Id);

                    userInRoles.Add(new UserInRole()
                    {
                        Role = role,

                        User = user,

                        RoleId = role.Id,

                        UserId = user.Id

                    });
                }

                user.UserInRoles = userInRoles;

                _context.Users.Add(user);

                _context.SaveChanges();

                return new ResultDto<ResultRegisterUserDto>()
                {
                    Data = new ResultRegisterUserDto()
                    {
                        UserId = user.Id
                    },
                    IsSuccess = true,

                    Message = "ثبت نام کاربر با موفقیت انجام شد"
                };
            } catch (DbUpdateException ex)
            {
                Console.WriteLine(ex.Message);

                return new ResultDto<ResultRegisterUserDto>()
                {
                    Data = new ResultRegisterUserDto()
                    {
                        UserId = 0
                    },
                    IsSuccess = false,

                    Message = "کاربر با این ایمیل وجود دارد !"
                };
            }
            catch (ValidationException ex)
            {
                ValidationResult result = _validator.Validate(requestRegister);

                string resultMessage = "";

                foreach (var ITEM in result.Errors)
                {
                    resultMessage += "،" + ITEM.ErrorMessage;
                }

                Console.WriteLine(ex.Message);

                return new ResultDto<ResultRegisterUserDto>()
                {
                    Data = new ResultRegisterUserDto()
                    {
                        UserId = 0
                    },
                    IsSuccess = false,

                    Message = resultMessage

                };
            }
           
        }
    }
}