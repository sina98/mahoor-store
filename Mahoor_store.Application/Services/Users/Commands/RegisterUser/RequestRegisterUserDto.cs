﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Users.Commands.RegisterUser
{
    public class RequestRegisterUserDto
    {
        public string LName { get; set; }
        public string FName { get; set; }
        public string Email{ get; set; }
        public string PhoneNum { get; set; }

        public string Password { get; set; }
        
        public string RePassword { get; set; }
        public List<RolesInRegisterUserDto> Roles { get; set; }
    }
}