﻿using FluentValidation;

namespace Mahoor_store.Application.Services.Users.Commands.RegisterUser.Validation
{
    public class RegisterUserValidator : AbstractValidator<RequestRegisterUserDto>
    {
        public RegisterUserValidator()
        {
            RuleFor(x => x.FName).NotEmpty().WithMessage("فیلد نام را وارد کنید");
            RuleFor(x => x.LName).NotEmpty().WithMessage("فیلد نام خانوادگی را وارد کنید");;
            RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("فیلد ایمیل را به درستی وارد کنید");;
            RuleFor(x => x.PhoneNum).NotEmpty().WithMessage("فیلد شماره ی همراه را وارد کنید");
            RuleFor(x => x.Password).NotEmpty().WithMessage("فیلد رمز عبور را وارد کنید");
            RuleFor(x => x.RePassword).Equal(x => x.Password).WithMessage("رمز عبور و تکرار رمز عبور مطابقت ندارند");;

        }
    }
}