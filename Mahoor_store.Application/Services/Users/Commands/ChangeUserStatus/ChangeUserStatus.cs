﻿using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Users.Commands.ChangeUserStatus
{
    public class ChangeUserStatus : IChangeUserStatus
    {
        private readonly IDatabaseContext _context;

        public ChangeUserStatus(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto Execute(long id)
        {
            var user = _context.Users.Find(id);
            
            if (user == null)
            {
                return new ResultDto()
                {
                    Message = "کاربر یاقت نشد" ,
                    
                    IsSuccess = false
                };
            }
            
            
            user.IsActivate = !user.IsActivate;
            
            _context.SaveChanges();

            string userState = user.IsActivate ? "فعال" : "غیر فعال";
            
            return new ResultDto()
            {
                Message =  $"  شد {userState}"  ,
                    
                IsSuccess = true
            };
        }
    }
}

