﻿using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Users.Commands.ChangeUserStatus
{
    public interface IChangeUserStatus
    {
        ResultDto Execute(long id);
    }
}