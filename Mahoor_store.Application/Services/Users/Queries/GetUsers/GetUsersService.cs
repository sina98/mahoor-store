﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Users.Queries.GetUsers
{
    public class GetUsersService : IGetUsers
    {
        private readonly IDatabaseContext _context;

        public GetUsersService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultGetUsers Execute(RequestGetUserDto requestGetUserDto)
        {
            var users = _context.Users.AsQueryable();
            
            if (!string.IsNullOrWhiteSpace(requestGetUserDto.SearchKey))
            {
                users = users.Where(p =>
                p.LName.Contains(requestGetUserDto.SearchKey) && p.Email.Contains(requestGetUserDto.SearchKey));
            }

            int rowsCount = 0;

            var getUsers =  users.ToPaged(requestGetUserDto.Page, 20, out rowsCount).Select(p => new GetUserDto()
            {
                Id = p.Id,

                Email = p.Email,

                FName = p.FName,

                LName = p.LName ,
                
                IsActivate = p.IsActivate ,
                
            }).ToList();

            return new ResultGetUsers()
            {
                Rows = rowsCount ,
                
                Users = getUsers
            };
        } 
    }
}