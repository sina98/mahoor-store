﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Users.Queries.GetUsers
{
    public class ResultGetUsers
    {
        public List<GetUserDto> Users { get; set; }

        public int Rows { get; set; }
    }
}