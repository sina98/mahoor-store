﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Users.Queries.GetUsers
{
    public interface IGetUsers
    {
        ResultGetUsers Execute(RequestGetUserDto requestGetUserDto);
    }
    
}