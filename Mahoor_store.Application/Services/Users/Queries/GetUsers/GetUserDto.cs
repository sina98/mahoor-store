﻿using Mahoor_store.Domain.Entities.Common;

namespace Mahoor_store.Application.Services.Users.Queries.GetUsers
{
    public class GetUserDto 
    {
        public long Id { get; set; }

        public string LName { get; set; }

        public string FName { get; set; }

        public string Email{ get; set; }
        
        public bool IsActivate { get; set; } = true;
        
        
    }
}