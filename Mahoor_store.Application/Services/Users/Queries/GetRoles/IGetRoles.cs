﻿using System.Collections.Generic;
using Mahoor_store.Application.Services.Users.Queries.GetUsers;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Users.Queries.GetRoles
{
    public interface IGetRoles
    {
        ResultDto<List<RolesDto>> Execute();
    }
}