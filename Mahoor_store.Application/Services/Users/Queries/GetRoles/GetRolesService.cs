﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Application.Services.Users.Queries.GetUsers;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Users.Queries.GetRoles
{
    public class GetRolesService :IGetRoles
    {
        private readonly IDatabaseContext _context;

        public GetRolesService(IDatabaseContext context)
        {
            _context = context;
        }
        
        List<RolesDto> rolesList = new List<RolesDto>();
        public ResultDto<List<RolesDto>> Execute()
        {
            var roles = _context.Roles.AsQueryable();

            rolesList =  roles.Select(p => new RolesDto()
            {
                Id = p.Id,

                Name = p.Name
            }).ToList();
            
            return new ResultDto<List<RolesDto>>()
            {
                Data = rolesList ,
                
                IsSuccess = true ,
                
                Message = ""
            };
        }
    }
}