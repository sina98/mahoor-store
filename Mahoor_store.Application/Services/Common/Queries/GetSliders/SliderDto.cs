﻿namespace Mahoor_store.Application.Services.Common.Queries.GetSliders
{
    public class SliderDto
    {
        public string Src { get; set; }

        public string Link { get; set; }
    }
}