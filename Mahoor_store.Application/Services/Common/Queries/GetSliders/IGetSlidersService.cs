﻿using System.Collections.Generic;
using Mahoor_store.Common;
using Microsoft.AspNetCore.Http;

namespace Mahoor_store.Application.Services.Common.Queries.GetSliders
{
    public interface IGetSlidersService
    {
        ResultDto<List<SliderDto>> Execute();
    }
}