﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Common.Queries.GetSliders
{
    public class GetSlidersService : IGetSlidersService
    {
        private readonly IDatabaseContext _context;

        public GetSlidersService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<List<SliderDto>> Execute()
        {
            var sliders = _context.Sliders
                .OrderByDescending(p => p.Id)
                .ToList()
                .Select(p => new SliderDto()
                {
                    Link = p.Link,
                    Src = p.Src
                }).ToList();
            
            return new ResultDto<List<SliderDto>>()
            {
                Data = sliders ,
                Message = "Success" ,
                IsSuccess = true
            };
        }
    }
}