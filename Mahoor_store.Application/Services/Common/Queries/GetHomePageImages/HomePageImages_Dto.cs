﻿using Mahoor_store.Domain.Entities.Home_Page;

namespace Mahoor_store.Application.Services.Common.Queries.GetHomePageImages
{
    public class HomePageImages_Dto
    {
        public long Id { get; set; }

        public string Src { get; set; }

        public string Link { get; set; }

        public HomePageImageLocation Location { get; set; }
    }
}