﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Common.Queries.GetHomePageImages
{
    public class GetHomePageImageService : IGetHomePageImagesService
    {

        private readonly IDatabaseContext _context;

        public GetHomePageImageService(IDatabaseContext context)
        {
            _context = context;
        }
        
        public ResultDto<List<HomePageImages_Dto>> Execute()
        {
            var images = _context.HomePageImages
                .OrderByDescending(p => p.Id)
                .ToList()
                .Select(p => new HomePageImages_Dto()
                {
                    Id = p.Id,
                    Link = p.Link,
                    Src = p.Src,
                    Location = p.Location
                })
                .ToList();
            
            return new ResultDto<List<HomePageImages_Dto>>()
            {
                Data = images ,
                Message = "Success",
                IsSuccess = true
            };
        }
    }
}