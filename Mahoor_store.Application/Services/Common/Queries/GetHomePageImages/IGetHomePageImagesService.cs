﻿using System.Collections.Generic;
using FluentValidation;
using Mahoor_store.Common;

namespace Mahoor_store.Application.Services.Common.Queries.GetHomePageImages
{
    public interface IGetHomePageImagesService
    {
        ResultDto<List<HomePageImages_Dto>> Execute();
    }
}