﻿using System.Collections.Generic;
using Mahoor_store.Common;
using Mahoor_store.Domain.Entities.Products;

namespace Mahoor_store.Application.Services.Common
{
    public interface IMenuItemService
    {
        ResultDto<List<MenuDto>> Execute();
    }
}