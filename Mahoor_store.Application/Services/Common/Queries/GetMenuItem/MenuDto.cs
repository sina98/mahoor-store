﻿using System.Collections.Generic;

namespace Mahoor_store.Application.Services.Common
{
    public class MenuDto
    {
        public long CatId { get; set; }

        public string Name { get; set; }

        public List<MenuDto> Childs { get; set; }
    }
}