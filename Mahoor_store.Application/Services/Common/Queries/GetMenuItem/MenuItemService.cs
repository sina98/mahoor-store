﻿using System.Collections.Generic;
using System.Linq;
using Mahoor_store.Application.Interfaces;
using Mahoor_store.Common;
using Microsoft.EntityFrameworkCore;

namespace Mahoor_store.Application.Services.Common
{
    public class MenuItemService : IMenuItemService
    {
        private readonly IDatabaseContext _context;

        public MenuItemService(IDatabaseContext context)
        {
            _context = context;
        }

        public ResultDto<List<MenuDto>> Execute()
        {
            var categories = _context.Categories
                .Include(p => p.SubCategory)
                .ToList()
                .Select(p => new MenuDto()
                {
                    Name = p.Name,
                    CatId = p.Id,
                    Childs = p.SubCategory.ToList().Select(child => new MenuDto()
                    {
                        Name = child.Name,
                        CatId = child.Id
                    }).ToList()
                }).ToList();
            return new ResultDto<List<MenuDto>>()
            {
                Data = categories ,
                Message = "Success",
                IsSuccess = true
            };
        }
        
    }
}