﻿namespace Mahoor_store.Common.Roles
{
    public class UserRoles
    {
        public const string Admin = "Admin";

        public const string Customer = "Customer";
        
        public const string Operator = "Operator";
    }
}